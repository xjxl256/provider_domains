# Email Provider Domains #

### What is this repository for? ###

* Provides a list of domains linked to email service providers.
* Customers that share a domain on this list should NOT be assumed to be from the same company
* Base list source: https://gist.github.com/tbrianjones/5992856
* Base list is not up-to-date, so modifications will be maintained in this repo
* Email domain type (based on this list) is currently an important feature in the Acquisition CLV model

### How do I get set up? ###



### Contribution guidelines ###

* To suggest edits, please create a new branch and provide an explanation in pull request for why domain(s) should be added or removed
* Add Jay Larson as approver on pull request
